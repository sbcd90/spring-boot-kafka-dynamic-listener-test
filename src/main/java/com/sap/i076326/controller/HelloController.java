package com.sap.i076326.controller;

import com.sap.i076326.kafka.DynamicListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private DynamicListener listener;

    @RequestMapping("/")
    public String index() {
        this.listener.newContainer("TestTopic", 0);
        return "Hello World";
    }
}