package com.sap.i076326.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SimpleKafkaProducer {
    private static final String KAFKA_TOPIC = "TestTopic";

    private static final String KAFKA_BROKERS = "****";

    private static final String KAFKA_USERNAME = "****";

    private static final String KAFKA_SECRET = "****";

    private Map<String, Object> props;

    private KafkaProducer<String, String> kafkaProducer;

    public SimpleKafkaProducer() {
        this.props = new HashMap<>();

        this.props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
        this.props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        this.props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");

        String jaasTemplate = "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";";
        String jaasConfig = String.format(jaasTemplate, KAFKA_USERNAME, KAFKA_SECRET);

        this.props.put("security.protocol", "SASL_SSL");
        this.props.put("sasl.mechanism", "PLAIN");
        this.props.put("sasl.jaas.config", jaasConfig);

        this.kafkaProducer = new KafkaProducer<String, String>(this.props);
    }

    public void sendRecords(String key, String value) throws Exception {

//        for (int count = 0; count < 10; count++) {
        int partition = new Random().nextInt(12);

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastFetchTimestamp = dateFormatter.format(date);

        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(KAFKA_TOPIC, 0,
                "hello",  "world");
        this.kafkaProducer.send(producerRecord);
//            Thread.sleep(4000);
//        }
        this.kafkaProducer.close();
    }

    public static void main(String[] args) throws Exception {
        SimpleKafkaProducer kafkaProducer = new SimpleKafkaProducer();
        kafkaProducer.sendRecords("hello", "world");
    }
}