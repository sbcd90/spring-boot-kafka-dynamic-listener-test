spring-boot-kafka-dynamic-listener-test
=======================================

Steps
=====

- Create a topic named `TestTopic` in Kafka.

- Add kafka credentials in `consumerFactory` bean & `SimpleKafkaProducer` constants.

- Run `SimpleKafkaProducer` twice to publish 2 msgs.

- Start spring boot app `Application`

- It should receive 2 msgs in the listener [here](src/main/java/com/sap/i076326/kafka/DynamicListener.java)#L35-#L37.

- It should stop container after 5 secs [here](src/main/java/com/sap/i076326/kafka/DynamicListener.java)#L45-#L57

- Run `SimpleKafkaProducer` again twice to publish 2 msgs.
These msgs wont be received as container stopped.
  
- Create a new container by calling `GET http://localhost:8080/`. Look [here](src/main/java/com/sap/i076326/controller/HelloController.java)#L15-#L18

- It should receive 2 msgs in the listener [here](src/main/java/com/sap/i076326/kafka/DynamicListener.java)#L35-#L37.

- It should stop container after 5 secs [here](src/main/java/com/sap/i076326/kafka/DynamicListener.java)#L45-#L57 again.
